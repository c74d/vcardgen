extern crate vcardgen;

#[macro_use]
extern crate clap;

use std::io;
use std::io::Write;

fn main() {
    let args = app_from_crate!()
        .help_message(
            "Prints help information (NOTE: `-h` prints less, `--help` prints more)",
        )
        .arg(
            clap::Arg::with_name("buffer-output")
                .long("buffer")
                .short("b")
                .value_name("KiB")
                .help("Limits the size of the output buffer")
                .long_help(
                    "Limits the amount of output to be buffered in memory to this many kibibytes. \
                     Approximately, when the amount of output held in memory reaches this many \
                     kibibytes, that output will be written out. By default, if this option is not \
                     used, the buffer size is limited only by the amount of available memory. If \
                     this option is not used, no vCards will be emitted if an error is \
                     encountered, whereas, if this option is used, incomplete output may be \
                     emitted if an error is encountered.",
                )
                .validator(|arg| {
                    arg.parse::<usize>().map(|_| ()).map_err(
                        |err| err.to_string(),
                    )
                }),
        )
        .get_matches();

    let buffer_size = args.value_of_lossy("buffer-output").map(|arg| {
        1024 *
            arg.parse::<usize>().expect(
                "This should already have been \
                 validated.",
            )
    });

    let stdin = io::stdin();
    let stdout = io::stdout();
    let locked_stdin = stdin.lock();
    let buffered_stdin = io::BufReader::new(locked_stdin);

    let mut output = match buffer_size {
        Some(size) => OutputWriter::Stream(io::BufWriter::with_capacity(size, stdout.lock())),
        None => OutputWriter::Buffer(Vec::new()),
    };

    match vcardgen::read_and_write(buffered_stdin, output.trait_obj()) {
        Ok(done) => done,
        Err(err) => panic!("Transformation failed: {}", err),
    }

    match output {
        OutputWriter::Stream(_) => {}
        OutputWriter::Buffer(ref buffer) => {
            io::stdout().write_all(buffer).expect(
                "Writing output failed",
            )
        }
    }
}

enum OutputWriter<StreamWriter>
where
    StreamWriter: Write,
{
    Stream(io::BufWriter<StreamWriter>),
    Buffer(Vec<u8>),
}

impl<StreamWriter> OutputWriter<StreamWriter>
where
    StreamWriter: Write,
{
    fn trait_obj(&mut self) -> &mut Write {
        match self {
            &mut OutputWriter::Stream(ref mut s) => s,
            &mut OutputWriter::Buffer(ref mut b) => b,
        }
    }
}
