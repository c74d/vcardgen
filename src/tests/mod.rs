use super::*;

#[test]
fn capitalize_first_char_1() {
    assert_eq!(capitalize_first_char(""), "");
    assert_eq!(capitalize_first_char(" "), " ");
    assert_eq!(capitalize_first_char("_"), "_");
    assert_eq!(capitalize_first_char("A"), "A");
    assert_eq!(capitalize_first_char("Z"), "Z");
    assert_eq!(capitalize_first_char(" a"), " a");
    assert_eq!(capitalize_first_char(" z"), " z");
    assert_eq!(capitalize_first_char("a"), "A");
    assert_eq!(capitalize_first_char("z"), "Z");
    assert_eq!(capitalize_first_char("abc"), "Abc");
    assert_eq!(capitalize_first_char("xyz"), "Xyz");
    assert_eq!(capitalize_first_char("Lorem ipsum"), "Lorem ipsum");
    assert_eq!(capitalize_first_char("lorem ipsum"), "Lorem ipsum");
}

#[test]
fn contact_full_1() {
    let test_input = include_str!("contacts-1-input.yaml");
    let test_output = include_str!("contacts-1-output.vcf");
    let mut actual_output = Vec::<u8>::new();

    read_and_write(test_input.as_bytes(), &mut actual_output).unwrap();

    let actual_output_string = String::from_utf8(actual_output).unwrap();

    assert_eq!(test_output, actual_output_string);
}
