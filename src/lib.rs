#![deny(warnings)]
#![forbid(unsafe_code)]

extern crate itertools;
extern crate smallvec;

#[macro_use]
extern crate error_chain;

#[macro_use]
extern crate yamlette;

#[cfg(test)]
#[macro_use]
extern crate pretty_assertions;

use itertools::Itertools;
use smallvec::Array;
use smallvec::SmallVec;
use std::borrow::Cow;
use std::fmt;
use std::io;
use std::io::prelude::*;
use std::iter;
use std::ops::Deref;
use std::str;
use yamlette::book::extractor::pointer::Pointer;
use yamlette::book::extractor::traits::FromPointer;

#[cfg(test)]
mod tests;

error_chain! {
    foreign_links {
        Io(io::Error);
    }

    errors {
        CitationNeeded(name: String) {
            description("one or more contacts are specified with no source for their data")
            display("No source is specified for the data for contact {:?}. Contact data must be \
                     cited; i.e., the source of the data must be specified, either for individual \
                     contacts or for a whole file of contacts.",
                     name)
        }
        DataSourceIsMultiline(name: String) {
            description("one or more contacts have a data source that includes line-breaks")
            display("The data source for contact {:?}, or for the whole file of contacts, \
                     includes one or more line-feed characters, which is forbidden.",
                     name)
        }
    }
}

#[derive(Debug, Default, Eq, Ord, PartialEq, PartialOrd)]
struct SmallByteString<A>(SmallVec<A>)
where
    A: Array<Item = u8>;

type Sbs16 = SmallByteString<[u8; 16]>;

type Sbs32 = SmallByteString<[u8; 32]>;

#[derive(Debug)]
struct Contacts {
    contacts: Vec<Contact>,
    default_data_src: String,
}

#[derive(Debug, Eq, Ord, PartialEq, PartialOrd)]
struct Contact {
    name_family: Sbs16,
    name_given: Sbs16,
    name_middle: Sbs16,
    name_prefix: Sbs16,
    name_suffix: Sbs16,
    org: Sbs16,
    job: Sbs16,
    email: Vec<EmailAddr>,
    tel: Vec<TelNum>,
    addr: Vec<StreetAddr>,
    parents: Vec<Sbs16>,
    siblings: Vec<Sbs16>,
    spouse: Vec<Sbs16>,
    children: Vec<Sbs16>,
    notes: Vec<String>,
    date_sourced: Sbs16,
    data_source: Option<String>,
}

#[derive(Debug, Eq, Ord, PartialEq, PartialOrd)]
struct EmailAddr {
    ty: Sbs16,
    addr: Sbs32,
}

#[derive(Debug, Eq, Ord, PartialEq, PartialOrd)]
struct TelNum {
    ty: Sbs16,
    num: Sbs16,
}

#[derive(Debug, Eq, Ord, PartialEq, PartialOrd)]
struct StreetAddr {
    ty: Sbs16,
    street: Sbs32,
    city: Sbs16,
    region: Sbs16,
    postal_code: Sbs16,
    country: Sbs16,
}

impl Contacts {
    fn from_yaml(yaml_input: String) -> Result<Self> {
        yamlette!(read; yaml_input; [[
            {
                "data source" => (data_src: String),
                "contacts" => (list contacts: Vec<Contact>)
            }
        ]]);

        Ok(Contacts {
            contacts: contacts.unwrap_or_default(),
            default_data_src: data_src.unwrap_or_default().trim().to_owned(),
        })
    }

    fn from_reader<R>(mut input_reader: R) -> Result<Self>
    where
        R: Read,
    {
        let mut text = String::new();
        input_reader.read_to_string(&mut text)?;
        Self::from_yaml(text)
    }

    fn write_as_vcards<W>(&self, mut output_writer: W) -> Result<()>
    where
        W: Write,
    {
        for contact in &self.contacts {
            contact.write_as_vcard(
                &mut output_writer,
                &self.default_data_src,
            )?
        }

        output_writer.flush()?;

        Ok(())
    }
}

impl Contact {
    fn write_as_vcard<W>(&self, mut output_writer: W, default_data_src: &str) -> Result<()>
    where
        W: Write,
    {
        let &Contact {
            ref name_family,
            ref name_given,
            ref name_middle,
            ref name_prefix,
            ref name_suffix,
            ref org,
            ref job,
            ref email,
            ref tel,
            ref addr,
            ref parents,
            ref siblings,
            ref spouse,
            ref children,
            ref notes,
            date_sourced: _,
            data_source: _,
        } = self;

        let name_full = [
            name_prefix,
            name_given,
            name_middle,
            name_family,
            name_suffix,
        ].iter()
            .filter(|s| !s.is_empty())
            .join(" ");

        write!(output_writer, "BEGIN:VCARD\r\nVERSION:2.1\r\n")?;
        write!(
            output_writer,
            "N:{};{};{};{};{}\r\n",
            name_family,
            name_given,
            name_middle,
            name_prefix,
            name_suffix
        )?;

        if !org.is_empty() {
            write!(output_writer, "ORG:{}\r\n", org)?
        }

        if !job.is_empty() {
            write!(output_writer, "TITLE:{}\r\n", job)?
        }

        for &EmailAddr { ref ty, ref addr } in email {
            write!(output_writer, "EMAIL;TYPE={}:{}\r\n", ty, addr)?
        }

        for &TelNum { ref ty, ref num } in tel {
            write!(output_writer, "TEL;TYPE={}:{}\r\n", ty, num)?
        }

        for &StreetAddr {
            ref ty,
            ref street,
            ref city,
            ref region,
            ref postal_code,
            ref country,
        } in addr
        {
            write!(
                output_writer,
                "ADR;TYPE={ty}:;;{street};{city};{region};{postal_code};{country}\r\n",
                ty = ty,
                street = street,
                city = city,
                region = region,
                postal_code = postal_code,
                country = country
            )?
        }

        for name in parents {
            write!(
                output_writer,
                "X-ANDROID-CUSTOM:vnd.android.cursor.item/relation;{};9;;;;;;;;;;;;;\r\n",
                name
            )?
        }

        for name in siblings {
            write!(
                output_writer,
                "X-ANDROID-CUSTOM:vnd.android.cursor.item/relation;{};0;Sibling;;;;;;;;;;;;\r\n",
                name
            )?
        }

        for name in spouse {
            write!(
                output_writer,
                "X-ANDROID-CUSTOM:vnd.android.cursor.item/relation;{};14;;;;;;;;;;;;;\r\n",
                name
            )?
        }

        for name in children {
            write!(
                output_writer,
                "X-ANDROID-CUSTOM:vnd.android.cursor.item/relation;{};3;;;;;;;;;;;;;\r\n",
                name
            )?
        }

        let citation = self.citation(default_data_src, &name_full)?;
        let notes_text = iter::once(&citation).chain(notes).join("\r\n\r\n");
        // let notes_encoded = base64::encode_config(
        //     &notes_text,
        //     base64::Config::new(
        //         base64::CharacterSet::Standard,
        //         true,
        //         true,
        //         // base64::LineWrap::Wrap(72, base64::LineEnding::LF)
        //         base64::LineWrap::NoWrap
        //     )
        // );
        let notes_encoded = notes_text.replace("\r\n", "=0D=0A");
        write!(
            output_writer,
            "NOTE;ENCODING=QUOTED-PRINTABLE:{}\r\n",
            notes_encoded
        )?;
        // for line in notes_encoded.split("\n") {
        //     write!(output_writer, " {}\r\n", line)?
        // }

        write!(output_writer, "END:VCARD\r\n\r\n")?;

        Ok(())
    }

    fn citation(&self, default_data_src: &str, name_full: &str) -> Result<String> {
        let mut parts = Vec::new();

        let &Contact {
            name_family: _,
            name_given: _,
            name_middle: _,
            name_prefix: _,
            name_suffix: _,
            ref org,
            ref job,
            ref email,
            ref tel,
            ref addr,
            ref parents,
            ref siblings,
            ref spouse,
            ref children,
            notes: _,
            ref date_sourced,
            ref data_source,
        } = self;

        fn add<Item, DispF, S1>(
            parts: &mut Vec<Cow<'static, str>>,
            desc: S1,
            values: &[Item],
            display: DispF,
        ) where
            DispF: Fn(&Item) -> String,
            S1: Into<Cow<'static, str>>,
        {
            if values.is_empty() {
                return;
            }

            let labels = values
                .iter()
                .map(display)
                .filter(|s| !s.is_empty())
                .collect::<Vec<_>>();

            if labels.is_empty() {
                return;
            }

            parts.push(", ".into());
            parts.push(desc.into());
            parts.push(" ".into());
            parts.push(format!("{:?}", labels).into());
        }

        add(&mut parts, "email addresses", email, |email| {
            email.addr.to_string()
        });

        add(
            &mut parts,
            "telephone numbers",
            tel,
            |tel| tel.num.to_string(),
        );

        add(&mut parts, "street addresses", addr, |addr| {
            addr.street.to_string()
        });

        add(&mut parts, "organization", &[org], |org| org.to_string());

        add(&mut parts, "occupation", &[job], |job| job.to_string());

        add(&mut parts, "parents", parents, |parent| parent.to_string());

        add(&mut parts, "spouse", spouse, |spouse| spouse.to_string());

        add(
            &mut parts,
            "siblings",
            siblings,
            |sibling| sibling.to_string(),
        );

        add(&mut parts, "children", children, |child| child.to_string());

        if let Some((first, rest)) = parts.split_first_mut() {
            *first = format!("[{}] ", date_sourced).into();
            if let Some((second, _rest)) = rest.split_first_mut() {
                *second = capitalize_first_char(second).into();
            }
        } else {
            return Ok(String::new());
        }

        // Add the word "and" before the last item in the list of properties sourced from the data
        // source.
        for part in parts.iter_mut().rev() {
            if part == ", " {
                *part = ", and ".into();
                break;
            }
        }

        // If there are only two properties listed, delete the comma between them.
        for part in parts.iter_mut() {
            if part == ", " {
                // There are more than two properties listed.
                break;
            }
            if part == ", and " {
                // There are only two properties listed.
                *part = " and ".into()
            }
        }

        let data_src = data_source.as_ref().map(|s| s.trim()).unwrap_or(
            default_data_src,
        );

        ensure!(
            !data_src.is_empty(),
            ErrorKind::CitationNeeded(name_full.to_owned())
        );

        ensure!(
            !data_src.contains("\n"),
            ErrorKind::DataSourceIsMultiline(name_full.to_owned())
        );

        Ok(
            parts
                .iter()
                .chain(&[" sourced from ".into(), data_src.into(), ".".into()])
                .join(""),
        )
    }
}

fn capitalize_first_char(s: &str) -> String {
    s.splitn(3, "")
        .enumerate()
        .map(|(index, substr)| match index {
            1 => substr.to_uppercase(),
            _ => substr.to_owned(),
        })
        .join("")
}

impl<'a> FromPointer<'a> for Contact {
    fn from_pointer(pointer: Pointer<'a>) -> Option<Self> {
        yamlette_reckon!(ptr; Some(pointer); {
            "name" => {
                "family" => (name_family: Sbs16),
                "given" => (name_given: Sbs16),
                "middle" => (name_middle: Sbs16),
                "prefix" => (name_prefix: Sbs16),
                "suffix" => (name_suffix: Sbs16)
            },
            "org" => (org: Sbs16),
            "job" => (job: Sbs16),
            "email" => (list email: Vec<EmailAddr>),
            "tel" => (list tel: Vec<TelNum>),
            "addr" => (list addr: Vec<StreetAddr>),
            "parents" => (list parents: Vec<Sbs16>),
            "siblings" => (list siblings: Vec<Sbs16>),
            "spouse" => (list spouse: Vec<Sbs16>),
            "children" => (list children: Vec<Sbs16>),
            "notes" => (list notes: Vec<String>),
            "date sourced" => (date_sourced: Sbs16),
            "data source" => (data_source: String)
        });

        Some(Contact {
            name_family: name_family.unwrap_or_default(),
            name_given: name_given.unwrap_or_default(),
            name_middle: name_middle.unwrap_or_default(),
            name_prefix: name_prefix.unwrap_or_default(),
            name_suffix: name_suffix.unwrap_or_default(),
            org: org.unwrap_or_default(),
            job: job.unwrap_or_default(),
            email: email.unwrap_or_default(),
            tel: tel.unwrap_or_default(),
            addr: addr.unwrap_or_default(),
            parents: parents.unwrap_or_default(),
            siblings: siblings.unwrap_or_default(),
            spouse: spouse.unwrap_or_default(),
            children: children.unwrap_or_default(),
            notes: notes.unwrap_or_default(),
            date_sourced: date_sourced.unwrap_or_default(),
            data_source,
        })
    }
}

impl<'a> FromPointer<'a> for EmailAddr {
    fn from_pointer(pointer: Pointer<'a>) -> Option<Self> {
        yamlette_reckon!(ptr; Some(pointer); {
            "type" => (ty: Sbs16),
            "addr" => (addr: Sbs32)
        });

        Some(EmailAddr {
            ty: ty.unwrap_or_default(),
            addr: addr.unwrap_or_default(),
        })
    }
}

impl<'a> FromPointer<'a> for TelNum {
    fn from_pointer(pointer: Pointer<'a>) -> Option<Self> {
        yamlette_reckon!(ptr; Some(pointer); {
            "type" => (ty: Sbs16),
            "num" => (num: Sbs16)
        });

        Some(TelNum {
            ty: ty.unwrap_or_default(),
            num: num.unwrap_or_default(),
        })
    }
}

impl<'a> FromPointer<'a> for StreetAddr {
    fn from_pointer(pointer: Pointer<'a>) -> Option<Self> {
        yamlette_reckon!(ptr; Some(pointer); {
            "type" => (ty: Sbs16),
            "street" => (street: Sbs32),
            "city" => (city: Sbs16),
            "region" => (region: Sbs16),
            "postal code" => (postal_code: Sbs16),
            "country" => (country: Sbs16)
        });

        Some(StreetAddr {
            ty: ty.unwrap_or_default(),
            street: street.unwrap_or_default(),
            city: city.unwrap_or_default(),
            region: region.unwrap_or_default(),
            postal_code: postal_code.unwrap_or_default(),
            country: country.unwrap_or_default(),
        })
    }
}

impl<A> Deref for SmallByteString<A>
where
    A: Array<Item = u8>,
{
    type Target = [A::Item];

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl<A> fmt::Display for SmallByteString<A>
where
    A: Array<Item = u8>,
{
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        f.pad(str::from_utf8(self).expect(
            "From UTF-8 this came, and to UTF-8 it \
             must return.",
        ))
    }
}

impl<'a, A> FromPointer<'a> for SmallByteString<A>
where
    A: Array<Item = u8>,
{
    fn from_pointer(pointer: Pointer<'a>) -> Option<Self> {
        yamlette_reckon!(ptr; Some(pointer); (text: &str));
        if let Some(s) = text {
            return Some(SmallByteString(SmallVec::from_slice(s.as_bytes())));
        }

        yamlette_reckon!(ptr; Some(pointer); (integer: isize));
        if let Some(n) = integer {
            return Some(SmallByteString(
                SmallVec::from_slice(format!("{}", n).as_bytes()),
            ));
        }

        None
    }
}

pub fn read_and_write<R, W>(input_reader: R, output_writer: W) -> Result<()>
where
    R: Read,
    W: Write,
{
    Contacts::from_reader(input_reader)?.write_as_vcards(
        output_writer,
    )
}
